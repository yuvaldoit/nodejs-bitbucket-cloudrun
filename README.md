# Nodejs Hello World for Cloud Run.

## Running the app:
    npm install
    node index.js

## Setting up the GCP infrastructure:

### Enabling services:
    gcloud services enable cloudbuild.googleapis.com
    gcloud services enable run.googleapis.com
    gcloud services enable artifactregistry.googleapis.com
    gcloud services enable cloudresourcemanager.googleapis.com
    gcloud services enable compute.googleapis.com

### Connecting Bitbucket repository:
As described in <https://cloud.google.com/source-repositories/docs/mirroring-a-bitbucket-repository>

### Creating container images repository using Artifact Repository:
    gcloud artifacts repositories create --repository-format=docker --location=[region] [artifact_repository_repo_name]

### Assigning Cloud Run role for the Cloud Build service account:
    gcloud projects add-iam-policy-binding --member=serviceAccount:[project_number]@cloudbuild.gserviceaccount.com --role=roles/run.admin [project_id]

### Creating custom service account used by the Cloud Run service:
    gcloud iam service-accounts create [sa_name]

### Allowing Cloud Build service account to deploy Cloud Run service:
    gcloud iam service-accounts add-iam-policy-binding --member="serviceAccount:[project_number]@cloudbuild.gserviceaccount.com" --role="roles/iam.serviceAccountUser" [sa_name]@[project_id].iam.gserviceaccount.com

### Creating trigger to deploy code committed to the main branch:
**NOTE!** replace the [values] in `cloudbuild.yaml` file with actual values.

    gcloud beta builds triggers create cloud-source-repositories --repo=bitbucket_[bitbucket_user_name]_[repo_name] --branch-pattern=^master$ --build-config=cloudbuild.yaml

### Creating static IP for the load balancer:
    gcloud compute addresses create --ip-version=IPV4 --global [address_name]

### Creating Google managed certificate:
    gcloud compute ssl-certificates create --domains=[domain_name] --global [cert_name]

### Creating serverless network endpoint group:
    gcloud compute network-endpoint-groups create --region=[region] --network-endpoint-type=serverless --cloud-run-service=[cloud_run_service_name] [neg_name]

### Creating backend:
    gcloud compute backend-services create --global [backend_name]

### Creating backend service:
    gcloud compute backend-services add-backend --global --network-endpoint-group=[neg_name] --network-endpoint-group-region=[region] [backend_service_name]

### Creating url map:
    gcloud compute url-maps create --default-service=[backend_service_name] [map_name]

### Creating HTTPS proxy:
    gcloud compute target-https-proxies create --ssl-certificates=[cert_name] --url-map=[map_name] [proxy_name]

### Creating forwarding rule:
    gcloud compute forwarding-rules create --address=[address_name] --target-https-proxy=[proxy_name] --global --ports=443 [forwarding_rule_name]

### Allowing anyone to invoke the Cloud Run service:
    gcloud run services add-iam-policy-binding --platform=managed --region=[region] --member="allUsers" --role="roles/run.invoker" [cloud_run_service_name]
