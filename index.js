const express = require('express')
const app = express()
const port = process.env.PORT || 8080
const cloud_run_service = process.env.K_SERVICE
const cloud_run_revision = process.env.K_REVISION
const cloud_run_configuration = process.env.K_CONFIGURATION

app.get('/', (req, res) => {
  var message = 'Hello World\n'
  if (cloud_run_service) {
    message += `
Cloud Run:
==========
Service: ${cloud_run_service}
Revision: ${cloud_run_revision}
Configuration: ${cloud_run_configuration}
`
  }
  res.send(message)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
